package com.unilecta.unilecta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class UnilectaApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnilectaApplication.class, args);
	}

}
